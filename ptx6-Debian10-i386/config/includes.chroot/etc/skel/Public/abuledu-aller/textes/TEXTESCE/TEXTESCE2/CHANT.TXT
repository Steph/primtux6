Chant de printemps. 

Quand revient le gai printemps, la for�t s'�veille : 
Les oiseaux, � pleine voix, sifflent, trillent dans les bois. 
Quand revient le gai printemps, la for�t s'�veille. 

La fauvette et le pinson chantent � tue-t�te : 
La nature reverdit, bient�t reviendront les nids. 
La fauvette et le pinson chantent � tue-t�te. 

Fais comme eux, chante bien haut que la vie est belle : 
Aurais-tu plus de soucis, trop de peine ou trop d'ennuis? 
Fais comme eux, chante bien haut que la vie est belle. 

