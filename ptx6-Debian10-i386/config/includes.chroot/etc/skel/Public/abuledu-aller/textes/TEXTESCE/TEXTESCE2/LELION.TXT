Un lion comme �a.

Il y avait une fois, en Afrique, un petit n�gre qui habitait tout pr�s du d�sert. Il s'appelait Bamboul. Il �tait tr�s gentil. Sa maman lui disait toujours: � Bamboul ne vas pas dans le d�sert, il y a des gros lions qui te mangeraient tout cru.�
Mais Bamboul avait grande envie de se promener tout seul en tra�nant son petit chemin de fer, et un jour il est parti tout seul, dans le d�sert, avec son petit chemin de fer...
Il �tait content ; son petit chemin de fer roulait bien et Bamboul marchait, marchait...
Mais tout � coup, il pousse un grand cri. Que lui arrive-t-il ? Un �norme lion �tait devant lui. Bamboul s'arr�te, il a peur, il ne sait de quel c�t� fuir, il l�che la ficelle de son petit chemin de fer... Il pleure.
Mais le lion s'arr�te aussi, le regarde, fait une horrible grimace de d�go�t, grogne 
� Baah �, se retourne et s'en va.
Alors, le petit Bamboul essuie ses larmes, ramasse la ficelle de son petit chemin de fer... Il a tout racont� � sa maman, mais elle n'y a jamais rien compris.
Quant au lion, vous auriez pu le voir, quelques instants plus tard, dans, l'oasis la plus proche, manger de bon app�tit de larges feuilles et des fleurs et vous auriez pu  voir aussi tous ses amis se moquer de lui et se tordre de rire en le voyant ainsi se r�galer.
C'�tait un lion v�g�tarien !
C'�tait un lion comme �a.
