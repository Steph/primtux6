############################################################################
# Copyright (C) 2002 David Lucardi
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,USA.
############################################################################
# File  : fichier.php
# Author  : David Lucardi
#           mailto: DavidLucardi@aol.com
# Date    : 26/04/2002
# Licence : GNU/GPL Version 2 ou plus
#
# Description:
# ------------
#
# @version
# @author     David Lucardi
# @project
# @copyright  David Lucardi 26/04/2002
#
#
#########################################################################
#!/bin/sh
#mots.tcl
# Licence : GNU/GPL Version 2 ou plus
# \
exec wish "$0" ${1+"$@"}
global sysFont font2 listdata longlist1 bgl categorie essais auto couleur  c  startdirect user Homeconf repertoire  tabstartdirect Home initrep baseHome vitesse nbreu

source menus.tcl
source parser.tcl
source eval.tcl
source fonts.tcl
source path.tcl
source msg.tcl
source compagnon.tcl

proc cancelkey {A} {
set A ""
#focus .
}
###########################################################
#font2 : police de caract�re
#listdata : donn�es du jeu, dans une liste
#bgl, bgn : couleurs utilis�es
# longlist1 : nombre de mots
#essais nombre de tentatives sur l'item en cours
#categorie : variable pour la categorie

#set bgn #ffff80
set bgn white
set bgl #ff80c0
set font2 $sysFont(lb)
set essais 0
set auto 0
set couleur bisque3
set vitesse 1
set categorie ""
set startdirect 1
set nbreu 0

set filuser [lindex $argv 1]
set plateforme $tcl_platform(platform)
set ident $tcl_platform(user)
if {$plateforme == "unix"} {set ident $env(USER)}
initlog $plateforme $ident
inithome

#interface

set c .frame.c

. configure -background $bgn


frame .menu -height 40
pack .menu -side bottom -fill both

button .menu.b1 -image [image create photo final -file [file join sysdata debut.gif]] -command "main"
pack .menu.b1 -side right
button .menu.bb1 -image [image create photo speak -file [file join sysdata speak.gif]] -command "litout"
pack .menu.bb1 -side right
tux_commence

#frame .barre -width 40
#pack .barre -side left -fill both

text .text -yscrollcommand ".scroll set" -setgrid true -width 55 -height 17 -wrap word -background white -font $sysFont(l)
scrollbar .scroll -command ".text yview"
pack .scroll -side right -fill y
pack .text -expand yes -fill both



#ouverture du fichier de configuration pour r�cup�rer des variables
catch {
set f [open [file join $baseHome reglages $filuser] "r"]
set categorie [gets $f]
set repertoire [gets $f]
set aller [gets $f]
set aller_flash [lindex $aller 9]
close $f
set vitesse [lindex $aller_flash 0]
set startdirect [lindex $aller_flash 4]
#set lecture_mot [lindex $aller_flash 5]
#set lecture_mot_cache [lindex $aller_flash 6]
}

set initrep [file join $Home textes $repertoire]

#chargement du texte avec d�tection du mode
set auto [charge .text [file join $Home textes $repertoire $categorie]]
bind .text <ButtonRelease-1> "lire"
bind . <KeyPress> "cancelkey %A"
if {$auto == 1} {
set vitesse $tabaide(flash)
#set longchamp $tablongchamp(flash)
#set listevariable $tablistevariable(flash)
set startdirect $tabstartdirect(flash)
#set vitesse $tablecture_mot(flash)
#set lecture_mot_cache $tablecture_mot_cache(flash)
}

focus .text
.text configure -state disabled -selectbackground white -selectforeground black

wm title . "[mc {Flash}] $categorie - [lindex [lindex $listexo 9] 1]"
label .menu.titre -text "[lindex [lindex $listexo 9] 1] - [mc {Observe}]" -justify center
pack .menu.titre -side left -fill both -expand 1

#############################""""""
proc main {} {
global sysFont listdata categorie listerep c bgn essais auto couleur aide listaide listexo iwish user startdirect

set listdata {}

    if {$auto==0} {
    pauto .text
    } else {
    pmanuel .text
    }


catch {destroy .text}
catch {destroy .scroll}
.menu.b1 configure -image [image create photo final -file [file join sysdata fin.gif]] -command "fin"



    catch {destroy .menu.lab}
    catch {destroy .menu.titre}
    label .menu.lab -text [lindex [lindex $listexo 9] 2] -justify center
    pack .menu.lab -side left -fill both -expand 1

set what [mc {Clique sur les ?? et trouve le m�me mot dans la liste.}]
if {$startdirect == 0} {set what "[format [mc {Bonjour %1$s .}] [string map {.log \040} [file tail $user]]] $what"}
tux_exo $what
#wm geometry .wtux +200+340

frame .frame -width 640 -height 380 -background $bgn
pack .frame -side top -fill both -expand yes
wm geometry . +52+0

#scrollbar .frame.vscroll -command "$c yview"
canvas $c -width 640 -height 380 -background $bgn -highlightbackground $bgn




#pack .frame.vscroll -side right -fill y
pack $c -expand yes -fill both

set str [string map {\\ \040} $listdata]
regsub -all \040+ $listdata \040 listdata
regsub -all {[\{\}\(\)\\\"]} $listdata "" listdata

set listaide $str
set listerep $listdata
set leng [llength $listdata]

#on m�lange la liste
for {set i 1} {$i <= $leng} {incr i 1} {
  set t1 [expr int(rand()*$leng)]
  set t2 [expr int(rand()*$leng)]
  set tmp [lindex $listdata $t1]
  set listdata [lreplace $listdata $t1 $t1 [lindex $listdata $t2]]
  set listdata [lreplace $listdata $t2 $t2 $tmp]
  }
set nb $leng
	#regsub -all {[.!,;:?]} $listdata "" listdata

#On place l'image et ses �tiquettes de syllabes sur le canevas
place $c
}


############################################################################"
proc pauto {t} {
global sysFont listdata iwish filuser listemotscaches 
set plist [parse2 $t]
set tmp ""
    foreach phrase $plist {
    set tmp [concat $tmp $phrase]
    }

#on r�cup�re les mots � masquer
#for {set i 0} {$i < 10} {incr i} {
#     	set k [expr int(rand()*[llength $tmp])]
#		if {[lindex $tmp $k] != ""} {lappend listemotscaches [lindex $tmp $k]}
#	set tmp [lreplace $tmp $k $k]	
#}
set compt 0

	for {set i [llength $tmp]} {$i > 0} {incr i -1} {
     	set k [expr int(rand()*[llength $tmp])]
	if {[lindex $tmp $k] != "" && [string length [lindex $tmp $k]]>1 } {
	lappend listemotscaches [lindex $tmp $k]
	incr compt
	if {$compt >9} {break}
	}
	set tmp [lreplace $tmp $k $k]	
	}

   if {[llength $listemotscaches] < 4} {
   set answer [tk_messageBox -message [mc {Erreur de traitement ou texte trop court.}] -type ok -icon info] 
   exec $iwish aller.tcl $filuser &
   exit
   }


set listdata $listemotscaches
}

#######################################################################"
proc pmanuel {t} {
global sysFont couleur listdata
set plist ""
set listemotscaches ""
set tmp2 ""
set plist2 [parse2 $t]
    foreach phrase $plist2 {
    set tmp2 [concat $tmp2 $phrase]
    }

set liste [$t tag ranges $couleur]
    for {set i 0} {$i < [llength $liste]} {incr i 2} {
    set str [$t get [lindex $liste $i] [lindex $liste [expr $i + 1]]]
    #regsub -all \" $str \\\" str
    lappend plist $str        
    }
set tmp ""
    foreach phrase $plist {
    set tmp [concat $tmp $phrase]
    }
regsub -all {[��'\"\�\�\[\]\240]} $tmp " " tmp
regsub -all {[,;.!?:-]} $tmp " " tmp
regsub -all {[\(\)\/%]} $tmp " " tmp
regsub -all {[\040+]} $tmp " " tmp


set ttmp ""
foreach mot $tmp {
if {[lsearch -exact $tmp2 $mot] != -1} {lappend ttmp $mot}
}

regsub -all {[��'\"\�\�\[\]\240]} $ttmp " " ttmp
regsub -all {[,;.!?:-]} $ttmp " " ttmp
regsub -all {[\(\)\/%]} $ttmp " " ttmp
regsub -all {[\040+]} $ttmp " " ttmp
#on r�cup�re les mots � masquer
set compt 0

	for {set i [llength $ttmp]} {$i > 0} {incr i -1} {
     	set k [expr int(rand()*[llength $ttmp])]
	if {[lindex $ttmp $k] != "" && [string length [lindex $ttmp $k]]>1 } {
	lappend listemotscaches [lindex $ttmp $k]
	incr compt
	if {$compt >9} {break}
	}
	set ttmp [lreplace $ttmp $k $k]	
	}

   	if {[llength $listemotscaches] < 4} {
    	pauto $t
    	return
   	}
set listdata $listemotscaches
}


##################################################
#placement des images, textes sur le canevas
#################################################
proc place {c} {
# erreur : variable pour d�terminer � quel moment on peut afficher l'aide
global sysFont font2 listdata longlist1 bgl ale erreur listerep listepos essais auto couleur categorie aide longchamp lecture_mot lecture_mot_cache bonnereponse totalp

set erreur 0
set bonnereponse ""
#on efface tout et on place les �l�ments sur le canevas
$c delete all

#longlist1 sert � r�cup�rer les syllabes du mot � afficher
set list1 $listdata
set longlist1 [llength $list1]
catch {destroy .menu.bb1}
#.menu.bb1 configure -command "speaktexte [list $listerep]"

#on m�lange les syllabes
for {set i 0} {$i < $longlist1} {incr i 1} {
  set ale($i) $i
  }
for {set i 1} {$i <= $longlist1} {incr i 1} {
  set t1 [expr int(rand()*$longlist1)]
  set t2 [expr int(rand()*$longlist1)]
  set temp $ale($t1)
  set ale($t1) $ale($t2)
  set ale($t2) $temp
  }

#on d�termine la longueur de la +longue syllabe, pour la dimension des traits
set max 0
for {set i 0} {$i < $longlist1} {incr i 1} {
if {[string length [lindex $list1 $i]] > $max} {
	set max [string length [lindex $list1 $i]]
    }

}
set max [expr int($max) + 3]

#on place les syllabes et les traits
set listecoord {180 40 460 40 180 80 460 80 180 120 460 120 180 160 460 160 180 200 460 200}
set listecible {320 260}
set ypos0 20
for {set i 0} {$i < $longlist1} {incr i 1} {
  $c create text 0 0 -text [lindex $list1 $i] -tags source$i -font $font2
  $c coords source$i [lindex $listecoord [expr $i*2]] [lindex $listecoord [expr ($i*2) +1]]
$c bind source$i <ButtonRelease-1> "verif $c $i"
}

  $c create text 0 0 -text "  ???  " -tags cible -font $font2
  $c coords cible [lindex $listecible 0] [lindex $listecible 1]

$c bind cible <ButtonRelease-1> "affichemot $c"


#if {$lecture_mot_cache == "0" } {
catch {destroy .menu.bb1}
#}
set totalp [llength $listdata]
}

##############################################################"
# v�rification, lorsque l'on appuie sur le bouton v�rifier
################################################################
proc verif {c i} {
global sysFont listdata erreur categorie bonnereponse essais totalp nbreu
incr essais

set reponse [$c itemcget source$i -text]

#on compare avec la chaine initiale
if {$bonnereponse !=""} {
	if {[string compare $reponse $bonnereponse] == 0} {
	incr nbreu
	tux_reussi
	.wtux.speech configure -text "Bien jou�!\n$essais essai(s).\n$nbreu mot(s) trouv�(s) sur $totalp."
	bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"


	$c itemconfigure cible -text $bonnereponse -fill yellow
	$c itemconfigure source$i -fill yellow
	update
	after 1000
	$c itemconfigure cible -text "  ???  " -fill black
	$c itemconfigure source$i -fill black
	update
      	if {[testefin $nbreu $totalp $essais] == 1} {
			$c bind cible <ButtonRelease-1> ""
			for {set k 0} {$k < [llength $listdata]} {incr k 1} {
			$c bind source$k <ButtonRelease-1> ""
			}
		return
		}
     	} else {
      tux_echoue1
	.wtux.speech configure -text "C'est faux!\n$essais essai(s).\n$nbreu mot(s) trouv�(s) sur $totalp."
	bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"


	$c itemconfigure cible -text $bonnereponse -fill yellow
	$c itemconfigure source$i -fill red
	update
	after 1000
	update
	$c itemconfigure cible -text "  ???  " -fill black
	$c itemconfigure source$i -fill black
     	}
}
set bonnereponse ""
}


proc affichemot {c} {
global listdata bonnereponse essais totalp nbreu vitesse
tux_continue
.wtux.speech configure -text "$essais essai(s).\n$nbreu mot(s) trouv�(s) sur $totalp."
bind .wtux.speech <1> "speaktexte [list [.wtux.speech cget -text]]"

set i [expr int(rand()*[llength $listdata])]
$c itemconfigure cible -text [lindex $listdata $i]
after [expr $vitesse*300] {$c itemconfigure cible -text "  ???   "}
set bonnereponse [lindex $listdata $i]
}


if {$startdirect == 0 } {
main
}

proc fin {} {
global sysFont categorie user essais listdata listexo iwish filuser startdirect repertoire nbreu totalp vitesse
variable repertconf
    set str2 [format [mc {%1$s essai(s) pour %2$s mot(s).}] $essais $totalp]
    set str1 [mc {Flash}]
    #set score [expr ($compt*100)/([llength $listdata]+($essais -1))]
    set score [expr ($nbreu*100)/($totalp +($essais- $nbreu))]
switch $startdirect {
1 {set startconf [mc {Le texte est visible au debut}]}
0 {set startconf [mc {Le texte n'est pas visible au debut}]}
}

switch $vitesse {
1 {set vitesseconf [mc {Vitesse : rapide}]}
2 {set vitesseconf [mc {Vitesse : moyenne}]}
3 {set vitesseconf [mc {Vitesse : lente}]}
}



set exoconf [mc {Parametres :}]
set exoconf "$exoconf $startconf - "
set exoconf "$exoconf $vitesseconf"

#enregistreval [mc {Exercice de closure}]\040[lindex [lindex $listexo 0] 1] \173$categorie\175 $str2 $score $repertconf 0 $user $exoconf $repertoire
enregistreval Flash\040[lindex [lindex $listexo 9] 1] \173$categorie\175 $str2 $score $repertconf 9 $user $exoconf $repertoire
exec $iwish aller.tcl $filuser &
exit
}




proc testefin {nbreu totalp essais} {
    global sysFont categorie user nbmotscaches
    if {$nbreu >= $totalp} {
    catch {destroy .menu.lab}
    set str0 [mc {Exercice termine. }]
    set str2 [format [mc {%1$s essai(s) pour %2$s mot(s).}] $essais $totalp]
    set str1 [mc {Flash}]
    label .menu.lab -text "$str0 - $str2"
    pack .menu.lab
    bell
set score [expr ($nbreu*100)/($totalp +($essais- $nbreu))]
if {$score <50} {tux_triste $score}
if {$score >=50 && $score <75 } {tux_moyen $score}
if {$score >=75} {tux_content $score}
return 1	
    }
return 0
}

























