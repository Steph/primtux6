#########################
#procÚdures pour le son
proc soundcap {} {
global sound sysFont

if {$sound == 1} {

label .frame2.menu7 -text [mc {Associer un son}]
place .frame2.menu7 -x 300 -y 10

radiobutton .frame2.menu8 -text [mc {Pour la categorie}] -background #ffff80 -value 0 -variable optson -font $sysFont(s)
place .frame2.menu8 -x 300 -y 60

radiobutton .frame2.menu9 -text [mc {Pour l'image}] -background #ffff80 -value 1 -variable optson -font $sysFont(s)
place .frame2.menu9 -x 300 -y 110


radiobutton .frame2.menu10 -text [mc {Pour le mot principal}] -background #ffff80 -value 2 -variable optson -font $sysFont(s)
place .frame2.menu10 -x 300 -y 160

.frame2.menu9 select


button .frame2.b1 -text "|>" -command Play
button .frame2.b3 -text "X" -command Stop
button .frame2.b4 -text "O" -command Record -fg red

place .frame2.b1 -x 315 -y 210
place .frame2.b3 -x 340 -y 210
place .frame2.b4 -x 365 -y 210

}
}

proc Record {} {
global currentimg repbasecat Home categorie currentson flagrec
variable optson
.frame2.menu8 configure -state disabled
.frame2.menu9 configure -state disabled
.frame2.menu10 configure -state disabled
.frame2.b1 configure -state disabled
.frame2.b4 configure -state disabled


if {$currentimg !="" && $currentimg !="blank.gif"} {
   set ext .wav
   set son [lindex [split $currentimg .] 0] 
    set pref s_	 
switch $optson {
0 {set son $categorie}
1 {set son $pref[lindex [split $currentimg .] 0]}
2 {set son [lindex [split $currentimg .] 0]}
}
set currentson [file join $Home sons $son$ext]
#S_Record $currentson
S_Record "tmp.wav"
set flagrec 1

    } else {
    set answer [tk_messageBox -message [mc {Aucune image selectionnee. Ouvrez une categorie ou choisissez d'abord une image}] -type ok -icon info] 
.frame2.menu8 configure -state normal
.frame2.menu9 configure -state normal
.frame2.menu10 configure -state normal
.frame2.b1 configure -state normal
.frame2.b4 configure -state normal

  }

}

proc Play {} {
global currentimg repbasecat Home categorie currentson
variable optson

if {$currentimg !="" && $currentimg !="blank.gif"} {
    set ext .wav
    set pref s_
    switch $optson {
0 {set son $categorie}
1 {set son $pref[lindex [split $currentimg .] 0]}
2 {set son [lindex [split $currentimg .] 0]}
}

  if {[file exists [file join $Home sons $son$ext]] == 1} {
set currentson [file join $Home sons $son$ext]
S_Play $currentson
    }
    } else {
    set answer [tk_messageBox -message [mc {Aucune image selectionnee. Ouvrez une categorie ou choisissez d'abord une image}] -type ok -icon info] 
    }
}

proc Stop {} {
global currentson flagrec
if {$currentson ==""} {set currentson null.wav}
catch {
.frame2.menu8 configure -state normal
.frame2.menu9 configure -state normal
.frame2.menu10 configure -state normal
.frame2.b1 configure -state normal
.frame2.b4 configure -state normal
}
if {$flagrec == 0} {
S_Stop $currentson
} else {
set flagrec 0
S_Stop tmp.wav
file copy -force tmp.wav $currentson
file delete -force tmp.wav
}
}

proc enterstart {file} {
global tmpson
set tmpson $file
S_Play $tmpson
}

proc enterstop {} {
global tmpson
if {$tmpson ==""} {set tmpson null.wav}
S_Stop $tmpson
}


global sound currentson flagrec

set sound 1
set flagrec 0
set currentson ""

switch [info tclversion] {
    
8.4 {load [file join Snack1.dll]}
8.3 {load [file join Snack.dll]}

}

