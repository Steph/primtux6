import math
import subprocess

from quart import Quart, websocket, redirect, url_for, render_template, request


from pmenu.conf import Conf
from pmenu.menu import sessions, get_menu
from pmenu.request import DataBase
from pmenu.pagination import pagination
from pmenu.package import apt_install, apt_installed, apt_exist, apt_remove


app = Quart(
    __name__,
    template_folder='templates'
)

conf = Conf()


@app.route('/')
@app.route('/<session_name>')
async def main(session_name=None):
    if not session_name in sessions:
        session_name = 'prof'
    db = DataBase(conf)
    nb_apps = db.count_apps(session_name)
    if nb_apps == 0:
        return
    results_by_page = conf.results_by_page
    nb_pages = math.ceil(nb_apps / conf.results_by_page)
    
    if session_name == 'mini':
        nb_pages = 0
        results_by_page = -1
    apps = db.get_page(
        0,
        results_by_page,
        session_name
    ).fetchall()
    nb_apps = db.count_apps(session_name)
    categories = db.get_categories(session_name).fetchall()
    session = sessions[session_name]
    session['menu'] = get_menu(categories)
    return await render_template(
        'index.html',
        page_index=0,
        session=session,
        apps=apps,
        nb_apps=nb_apps,
        pagination=pagination(0, nb_pages)
    )


@app.route('/apps', methods=['GET'])
async def _apps():
    session_name = request.args['session']
    db = DataBase(conf)
    page_index = 0
    cat_name = ''
    sub_cat_name = ''
    sub_sub_cat_name = ''
    if 'page_index'in request.args:
        page_index = int(request.args['page_index'])
    if 'cat_name'in request.args:
        cat_name = request.args['cat_name']
    if 'sub_cat_name'in request.args:
        sub_cat_name = request.args['sub_cat_name']
    if 'sub_sub_cat_name'in request.args:
        sub_sub_cat_name = request.args['sub_sub_cat_name']
    nb_apps = db.count_apps_from_cat(
        session_name,
        cat_name,
        sub_cat_name,
        sub_sub_cat_name
    )
    if nb_apps == 0:
        return render_template(
            'apps.html',
            nb_apps=0
        )
    nb_pages = math.ceil(nb_apps / conf.results_by_page)
    apps = db.get_page_from_cat(
        page_index * conf.results_by_page,
        conf.results_by_page,
        session_name,
        cat_name,
        sub_cat_name,
        sub_sub_cat_name
    ).fetchall()
    session = sessions[session_name]
    return await render_template(
        'apps.html',
        page_index=page_index,
        session=session,
        cat_name=cat_name,
        sub_cat_name=sub_cat_name,
        sub_sub_cat_name=sub_sub_cat_name,
        apps=apps,
        nb_apps=nb_apps,
        pagination=pagination(page_index, nb_pages)
    )


@app.route('/app/<_id>')
async def _app(_id):
    session_name = request.args['session']
    session = sessions[session_name]
    db = DataBase(conf)
    app = db.get(_id)
    app['app_name']
    return await render_template(
        'app.html',
        session=session,
        app=app,
        in_depot=apt_exist(app['app_name']),
        is_installed=apt_installed(app['app_name'])
    )


@app.route('/launch/<_id>')
async def launch_app(_id):
    db = DataBase(conf)
    app = db.get(_id)
    subprocess.run(app['path'], shell=True, check=True)
    return 'launched'


@app.route('/install/<_id>')
async def install_app(_id):
    db = DataBase(conf)
    app = db.get(_id)
    apt_install(app['app_name'])
    return 'installed'


@app.websocket('/ws/install')
async def ws_install():
    data = await websocket.receive()
    db = DataBase(conf)
    app = db.get(data)
    # TODO : --dry-run on dev mode
    with subprocess.Popen(
        ['apt-get', 'install', '%s' % app['app_name']],
        stdout=subprocess.PIPE,
        bufsize=1,
        universal_newlines=True
    ) as process:
        for line in process.stdout:
            line = line.rstrip()
            print(f"line = {line}")
            await websocket.send(line)


@app.websocket('/ws/uninstall')
async def ws_uninstall():
    data = await websocket.receive()
    db = DataBase(conf)
    app = db.get(data)
    # TODO : --dry-run on dev mode
    with subprocess.Popen(
        ['apt-get', 'remove', '-f', '%s' % app['app_name']],
        stdout=subprocess.PIPE,
        bufsize=1,
        universal_newlines=True
    ) as process:
        for line in process.stdout:
            line = line.rstrip()
            print(f"line = {line}")
            await websocket.send(line)
        await websocket.send('end')
