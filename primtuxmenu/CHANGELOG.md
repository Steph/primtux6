# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased : 2.0]

### Added

- serveur Rust qui s'appui sur HTTP2 : actix
- utilisation de Yew pour la partie front
- mise en place de SEO
- mise en place de thème CSS (personnalisation de son espace)
- indexation ScoLOMFR : https://www.reseau-canope.fr/scolomfr/faq.html

## [Unreleased : 1.0]

### Added
- intégration de Nix
- intégration de gSpeech
- intégration d'un chat bot
- mise en place de tests unitaires
- mise en place d'intégration continue
- flake8 sur l'ensemble du code python
- harmonisation avec https://framagit.org/mothsart/components-mould
- suivre la RFC : https://framagit.org/mothsart/rfc-primtux-web-app
- a11y
- commandes admin en cli
- utilisation de cypress
- moteur d'aide interactif
- outil de statistiques d'utilisation
- comportement en mode "réseau"
- gestion des fautes d'orthographes dans la recherche

- ressources :
    - https://www.staedtler.com/fr/fr/mandala-creator
    - https://twitter.com/laclasse_demma/status/1277141893416210437/photo/1
    - http://www2.ac-lyon.fr/services/rhone/maitrise-de-langue/spip.php?article305&lang=fr
    - https://laclassedemallory.net/2020/06/24/porte-cle-memo-et-aide-en-maths-et-francais/
- s'assurer au lancement (ou via un daemon) que des logiciels n'ont pas été installé en dehors de Primtuxmenu (synaptic, apt etc.)


## [Unreleased : 0.2]

### Added

- utilisation de docker en mode dev
- mise en place d'une vrai interface d'admin :
    - installation multiple des applications avec barre de progression
    - filtrage des applications
    - ajout/modification/suppression d'application

- intégration des logiciels
- intégration dans Nginx
- création d'un paquet .deb
- i18n
- mise en place d'un vrai "moteur de recherche"
- liste d'imprim écran pour présenter le logiciel (sans installation)

## [Unreleased : 0.1]

### Added
- menu proche de https://classe-numerique.fr/recherche/
- integration de l'ensemble des applications
- donner une identité graphique forte
- ajout des descriptifs (cf https://primtux.fr/Documentation/site/logiciels-PrimTux-V5.ods)
- ajout d'une entrée de makefile pour créer un commit de changement de db

### Bugfix
- gérer le flavicon

## [0.x]

### Added
- permettre la suppression des logiciels (apt)
- menu dynamique
- pagination
- serveur Flask
- HTML orienté composants
- CSS au format BEM
- colorer les requètes SQL
- permettre l'installation des logiciels (apt)
