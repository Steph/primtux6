CREATE TABLE IF NOT EXISTS apps (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name varchar(255) NOT NULL, /* nom visible par l'utilisateur */
    app_name varchar(255) NOT NULL, /* désignation dans apt */
    icon_path varchar(2048) NOT NULL, /* chemin de l'icône */
    path varchar(2048) NOT NULL, /* chemin de lancement du logiciel. ex: /usr/bin/monsoft */
    generic varchar(255) NOT NULL, /* infos complémentaires */

    mini_cat_name varchar(255) NULL, /* précise le nom de la catégorie dans la session mini */
    mini_sub_cat_name varchar(255) NULL, /* précise le nom de la sous-catégorie dans la session mini */
    mini_sub_sub_cat_name varchar(255) NULL, /* précise le nom de la sous-sous-catégorie dans la session mini */

    super_cat_name varchar(255) NULL, /* précise le nom de la catégorie dans la session super */
    super_sub_cat_name varchar(255) NULL, /* précise le nom de la sous-catégorie dans la session super */
    super_sub_sub_cat_name varchar(255) NULL, /* précise le nom de la sous-sous-catégorie dans la session super */

    maxi_cat_name varchar(255) NULL, /* précise le nom de la catégorie dans la session maxi */
    maxi_sub_cat_name varchar(255) NULL, /* précise le nom de la sous-catégorie dans la session maxi */
    maxi_sub_sub_cat_name varchar(255) NULL, /* précise le nom de la sous-sous-catégorie dans la session maxi */

    prof_cat_name varchar(255) NULL, /* précise le nom de la catégorie dans la session prof */
    prof_sub_cat_name varchar(255) NULL, /* précise le nom de la sous-catégorie dans la session prof */
    prof_sub_sub_cat_name varchar(255) NULL, /* précise le nom de la sous-sous-catégorie dans la session prof */

    in_mini BOOLEAN NOT NULL CHECK (in_mini IN (0, 1)), /* présent ou non dans la session mini */
    in_super BOOLEAN NOT NULL CHECK (in_super IN (0, 1)), /* présent ou non dans la session super */
    in_maxi BOOLEAN NOT NULL CHECK (in_maxi IN (0, 1)), /* présent ou non dans la session maxi */
    in_prof BOOLEAN NOT NULL CHECK (in_prof IN (0, 1)), /* présent ou non dans la session prof */
    license varchar(255) NULL, /* licence (GPL, BSD ...) */
    UNIQUE(name) /* force le nom à être unique */
);

CREATE TABLE IF NOT EXISTS apps_detail (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    app_key INTEGER,
    description TEXT NULL,
    link varchar(2048) NOT NULL,
    FOREIGN KEY(app_key) REFERENCES apps(id)
);

CREATE TABLE IF NOT EXISTS categories (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    name varchar(255) NOT NULL,
    icon_path varchar(2048) NULL,
    level INTEGER,
    parent_id INTEGER,
    session varchar(10) NOT NULL,
    FOREIGN KEY(parent_id) REFERENCES categories(id)
    UNIQUE(name, session, level)
);

CREATE TABLE IF NOT EXISTS keywords (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    text varchar(255) NOT NULL
);

CREATE TABLE IF NOT EXISTS contains (
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    id_apps INTEGER,
    id_keywords INTEGER,
    FOREIGN KEY(id_apps) REFERENCES apps(id)
    FOREIGN KEY(id_keywords) REFERENCES keywords(id)
);
