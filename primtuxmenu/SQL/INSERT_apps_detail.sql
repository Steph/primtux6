INSERT INTO "apps_detail" VALUES(
    1,
    23,
    "Adapation du jeu <strong>A la campagne</strong> de la compilation d'applications Clicmenu.
L'activité consiste à constituer des collections d'objets selon une quantité donnée, de 1 à 10 ou de 10 à 20.",
    'GPL3',
    'https://framagit.org/philippe-dpt35/alacampagne'
);
