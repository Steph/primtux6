/* session mini */

INSERT INTO categories (name, session, level) VALUES ('Le langage', 'mini', 0);
INSERT INTO categories (name, session, level) VALUES ('Activités artistiques', 'mini', 0);
INSERT INTO categories (name, session, level) VALUES ('Structuration de la pensée', 'mini', 0);
INSERT INTO categories (name, session, level) VALUES ('Explorer le monde', 'mini', 0);

INSERT INTO categories (name, session, level, parent_id) VALUES ("Repérage dans le temps et l'espace", 'mini', 1, 4);

INSERT INTO categories (name, session, level, parent_id) VALUES ("Le vivant, la matière et les objets", 'mini', 1, 4);

INSERT INTO categories (name, session, level, parent_id) VALUES ("Utiliser, fabriquer, manipuler des objets", 'mini', 2, 6);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Utiliser des outils numériques", 'mini', 2, 6);

INSERT INTO categories (name, session, level) VALUES ('Activités ludo-éducatives', 'mini', 0);

/* session super */

INSERT INTO categories (name, session, level) VALUES ('Français', 'super', 0);

INSERT INTO categories (name, session, level, parent_id) VALUES ("Lecture", 'super', 1, 10);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Grammaire", 'super', 1, 10);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Conjugaison", 'super', 1, 10);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Orthographe", 'super', 1, 10);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Lexique", 'super', 1, 10);

INSERT INTO categories (name, session, level) VALUES ('Mathématiques', 'super', 0);

INSERT INTO categories (name, session, level, parent_id) VALUES ("Numération", 'super', 1, 16);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Calcul", 'super', 1, 16);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Grandeur et mesure", 'super', 1, 16);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Géométrie", 'super', 1, 16);

INSERT INTO categories (name, session, level) VALUES ('Langue vivante', 'super', 0);
INSERT INTO categories (name, session, level) VALUES ('Questionner le monde', 'super', 0);

INSERT INTO categories (name, session, level, parent_id) VALUES ("Le temps, l'espace, le vivant, matière et objets", 'super', 1, 22);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Programmation", 'super', 1, 22);

INSERT INTO categories (name, session, level) VALUES ('Enseignements artistiques', 'super', 0);

INSERT INTO categories (name, session, level, parent_id) VALUES ("Arts plastiques", 'super', 1, 25);
INSERT INTO categories (name, session, level, parent_id) VALUES ('Éducation Musicale', 'super', 1, 25);

INSERT INTO categories (name, session, level) VALUES ('EMC (Enseignement Moral et Civique', 'super', 0);
INSERT INTO categories (name, session, level) VALUES ('Activités ludo-éducatives', 'super', 0);

/* session maxi */

INSERT INTO categories (name, session, level) VALUES ('Français', 'maxi', 0);

INSERT INTO categories (name, session, level, parent_id) VALUES ("Lecture", 'maxi', 1, 30);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Grammaire", 'maxi', 1, 30);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Conjugaison", 'maxi', 1, 30);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Orthographe", 'maxi', 1, 30);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Lexique", 'maxi', 1, 30);

INSERT INTO categories (name, session, level) VALUES ('Mathématiques', 'maxi', 0);

INSERT INTO categories (name, session, level, parent_id) VALUES ("Numération", 'maxi', 1, 36);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Calcul", 'maxi', 1, 36);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Grandeur et mesure", 'maxi', 1, 36);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Géométrie", 'maxi', 1, 36);

INSERT INTO categories (name, session, level) VALUES ('Langue vivante', 'maxi', 0);
INSERT INTO categories (name, session, level) VALUES ('Questionner le monde', 'maxi', 0);

INSERT INTO categories (name, session, level, parent_id) VALUES ("Le temps, l'espace, le vivant, matière et objets", 'maxi', 1, 42);
INSERT INTO categories (name, session, level, parent_id) VALUES ("Programmation", 'maxi', 1, 42);

INSERT INTO categories (name, session, level) VALUES ('Enseignements artistiques', 'maxi', 0);

INSERT INTO categories (name, session, level, parent_id) VALUES ("Arts plastiques", 'maxi', 1, 45);
INSERT INTO categories (name, session, level, parent_id) VALUES ('Éducation Musicale', 'maxi', 1, 45);

INSERT INTO categories (name, session, level) VALUES ('EMC (Enseignement Moral et Civique', 'maxi', 0);
