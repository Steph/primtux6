import re
import inspect

has_pygments = True
try:
    from pygments import highlight
    from pygments.lexers import SqlLexer
    from pygments.formatters import TerminalTrueColorFormatter
except:
    has_pygments = False

import sqlite3


def print_pygmentyze(value):
    if not has_pygments:
        print(value)
        return
    print(highlight(
        value,
        SqlLexer(),
        TerminalTrueColorFormatter(style='native')
    ))


class DataBase:
    def __init__(self, conf):
        self.debug = conf.DEBUG
        self.db = sqlite3.connect(conf.db.path)
        self.db.row_factory = sqlite3.Row

    def _where_like(self, value=None, _and=False):
        _where = ''
        if value:
            _where = "WHERE k.text LIKE '" + value + "%'"
        if _and:
            _where = 'WHERE %s' % _where
        else:
            _where = ' AND %s' % _where
        return _where


    def _exec(self, request, commit=False):
        if self.debug:
            request = re.sub('\s+', ' ', request).strip()
            previous_frame = inspect.currentframe().f_back
            frame = inspect.getframeinfo(previous_frame)
            print_pygmentyze(
                'sql (%s) : %s' % (
                    frame[2],
                    request
                )
            )
        result = self.db.execute(request)
        if commit:
            self.db.commit()
        return result

    def get(self, _id):
        return self._exec(
            '''
            SELECT a.id, a.name, a.app_name, a.generic, 
            a.icon_path, a.path,
            a.in_mini, a.in_maxi, a.in_super, a.in_prof, a.license,
            d.description, d.link
            FROM apps AS a
            LEFT JOIN apps_detail AS d ON d.app_key = a.id
            WHERE a.id = "%s";
            ''' % _id
        ).fetchone()


    def count_apps(self, session=None, value=None):
        _where = ''
        if session:
            _where = "WHERE in_%s = 1" % session
        if value:
            _where += self._where_like(value, session)
            return self._exec(
                '''
                SELECT count(r.id)
                FROM apps AS r
                INNER JOIN contains AS c ON c.id_apps = r.id
                INNER JOIN keywords AS k ON k.id = c.id_keywords
                %s;
                ''' % _where
            ).fetchone()[0]
        return self._exec(
            '''
            SELECT count(id) FROM apps %s;
            ''' % _where
        ).fetchone()[0]


    def get_page(self, offset, nb, session=None, value=None):
        _where = ''
        if session:
            _where = "WHERE in_%s = 1" % session
        if value:
            _where += self._where_like(value, session)
            return self._exec(
                '''
                SELECT id, name, app_name, generic, icon_path,
                path, in_mini, in_maxi, in_super, in_prof
                FROM apps AS r
                INNER JOIN contains AS c ON c.id_apps = r.id
                INNER JOIN keywords AS k ON k.id = c.id_keywords
                %s
                LIMIT %s, %s;
                ''' % (_where, offset, nb)
            )
        return self._exec(
            '''
            SELECT id, name, app_name, generic, icon_path,
            path, in_mini, in_maxi, in_super, in_prof
            FROM apps %s LIMIT %s, %s;
            ''' % (_where, offset, nb)
        )


    def count_apps_from_cat(
        self,
        session,
        cat_name=None,
        sub_cat_name=None,
        sub_sub_cat_name=None
    ):
        _where = 'WHERE in_%s = 1' % session
        if cat_name:
            _where += ' AND %s_cat_name = "%s"' % (session, cat_name)
        if sub_cat_name:
            _where += ' AND %s_sub_cat_name = "%s"' % (
                session,
                sub_cat_name
            )
        if sub_sub_cat_name:
            _where += ' AND %s_sub_sub_cat_name = "%s"' % (
                session,
                sub_sub_cat_name
            )
        return self._exec(
            '''
            SELECT count(id) FROM apps %s;
            ''' % _where
        ).fetchone()[0]


    def get_page_from_cat(
        self,
        offset,
        nb,
        session,
        cat_name=None,
        sub_cat_name=None,
        sub_sub_cat_name=None
    ):
        _where = 'WHERE in_%s = 1' % session
        if cat_name:
            _where += ' AND %s_cat_name = "%s"' % (session, cat_name)
        if sub_cat_name:
            _where += ' AND %s_sub_cat_name = "%s"' % (
                session,
                sub_cat_name
            )
        if sub_sub_cat_name:
            _where += ' AND %s_sub_sub_cat_name = "%s"' % (
                session,
                sub_sub_cat_name
            )
        return self._exec(
            '''
            SELECT id, name, app_name, generic, icon_path, path
            FROM apps %s LIMIT %s, %s;
            ''' % (_where, offset, nb)
        )


    def get_categories(self, session):
        return self._exec(
            '''
            SELECT id, name, icon_path, level, session, parent_id
            FROM categories WHERE session = "%s";
            ''' % session
        )


    def app_exist(self, name, sessions, session):
        result = self._exec(
            '''
            SELECT %s, %s, %s, %s
            FROM apps WHERE name = "%s";
            ''' % (
                'in_%s' % sessions[0],
                'in_%s' % sessions[1],
                'in_%s' % sessions[2],
                session,
                name
            )
        ).fetchone()
        if not result:
            return None
        return {
            sessions[0]: result[0],
            sessions[1]: result[1],
            sessions[2]: result[2],
            session: result[3]
        }


    def add_app(
        self,
        sessions,
        app_name,
        name,
        icon_path,
        cmd,
        generic,
        session
    ):
        return self._exec(
            '''
            INSERT INTO apps
            (name, app_name, generic, icon_path, path, %s, %s, %s, %s)
            VALUES ("%s", "%s", "%s", "%s", '%s', 0, 0, 0, 1);
            ''' % (
                'in_%s' % sessions[0],
                'in_%s' % sessions[1],
                'in_%s' % sessions[2],
                session,
                name,
                app_name,
                generic,
                icon_path,
                cmd
            ),
            True
        ).lastrowid


    def update_app(self, name, session):
        return self._exec(
            '''
            UPDATE apps
            set %s = 1
            WHERE name ="%s";
            ''' % (
                session,
                name
            )
        )


    def add_keyword(self, keyword, id_apps):
        keywords_cursor = self._exec(
            '''
            SELECT k.id FROM keywords AS k
            WHERE k.text = "%s";
            ''' % keyword
        ).fetchone()
        if keywords_cursor:
            keywords_id = keywords_cursor[0]
        else:
            keywords_id = self._exec(
                '''
                INSERT INTO keywords(text) VALUES("%s");
                ''' % keyword
            ).lastrowid
        self._exec(
            '''
            INSERT INTO contains(id_apps, id_keywords)
            VALUES (%s, %s);
            ''' % (id_apps, keywords_id)
        )


    def get_keywords(self, value=None):
        return self._exec(
            '''
            SELECT k.text, count(k.id) FROM keywords AS k
            INNER JOIN contains AS c ON k.id = c.id_keywords
            %s
            GROUP BY k.id
            ORDER BY count(k.id) DESC
            LIMIT 20;
            ''' % self._where_like(value)
        )
