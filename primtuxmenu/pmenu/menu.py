sessions = {
    'mini': {
        'type': 'mini',
        'name': 'Session Mini',
        'cycle': 'cycle 1 : maternelle'
    },
    'super': {
        'type': 'super',
        'name': 'Session Super',
        'cycle': 'cycle 2 : CP, CE1 et CE2'
    },
    'maxi': {
        'type': 'maxi',
        'name': 'Session Maxi',
        'cycle': 'cycle 3 : CM1 et CM2'
    },
    'prof': {
        'type': 'prof',
        'name': 'Session Prof',
        'cycle': """accès à l'ensemble des 3 cycles"""
    }
}


def add_sub_menu(menu_items, menu, parent_id, sub_menu_items=None):
    for item in menu_items:
        if item['parent_id'] != parent_id:
            continue
        menu_item = {}
        menu_item['name'] = item['name']
        if not 'menu' in menu_item:
            menu_item['menu'] = []
        if sub_menu_items:
            add_sub_menu(
                sub_menu_items,
                menu_item['menu'],
                item['id'],
                None
            )
        menu.append(menu_item)


def get_menu(categories):
    menu = []
    menu_items = []
    sub_menu_items = []
    sub_sub_menu_items = []

    for cat in categories:
        if cat['level'] == 1:
            sub_menu_items.append(cat)
            continue
        if cat['level'] == 2:
            sub_sub_menu_items.append(cat)
            continue
        menu_items.append(cat)

    for item in menu_items:
        menu_item = {}
        menu_item['name'] = item['name']
        if not 'menu' in menu_item:
            menu_item['menu'] = []
        add_sub_menu(
            sub_menu_items,
            menu_item['menu'],
            item['id'],
            sub_sub_menu_items
        )
        menu.append(menu_item)

    return menu
