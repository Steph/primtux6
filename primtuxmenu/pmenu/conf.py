from os.path import join

from .debug import is_debug_mode


class DbConf:
    path = 'primtuxmenu.db'
    drop_path = join('SQL', 'DROP.sql')
    create_path = join('SQL', 'CREATE.sql')
    insert_apps_path = join('SQL', 'INSERT_apps.sql')
    insert_apps_detail_path = join('SQL', 'INSERT_apps_detail.sql')
    insert_categories_path = join('SQL', 'INSERT_categories.sql')
    update_path = join('SQL', 'UPDATE.sql')

    def __init__(self, debug=False):
        if debug:
            return


class Conf:
    sessions = ['super', 'mini', 'maxi', 'prof']
    results_by_page = 10

    def __init__(self, verbose=False):
        """Init Primtux store config"""
        self.verbose = verbose
        self.DEBUG = False
        if is_debug_mode():
            self.DEBUG = True
        self.assets = ''
        self.db = DbConf(self.DEBUG)
