DELTA = 2

def pagination(current, last):
    if last <= 1:
        return
    _range = []
    range_with_dots = []
    l = None
    for i in range(1, last + 1):
        if (
            i == 1 or i == last 
            or i >= (current - DELTA) 
            and i < (current + DELTA + 1)
        ):
            _range.append(i)
    for i in _range:
        if l:
            if (i - l) == 2:
                range_with_dots.append(l + 1)
            elif i - l != 1:
                range_with_dots.append('...')  
        range_with_dots.append(i)
        l = i
    return range_with_dots
