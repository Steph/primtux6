import subprocess

def _pop(args):
    proc = subprocess.Popen(
        args,
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True
    )
    return proc.communicate()


def apt_exist(app_name):
    result = _pop([ 'apt-cache search --names-only "^%s$"' % app_name ])
    if len(result[0]) > 1:
        return True
    return False


def apt_installed(app_name):
    result = _pop([ 'dpkg -s "%s"' % app_name])
    if len(result[0]) > 1:
        return True
    return False


def apt_install(app_name):
    proc = subprocess.Popen(
        ['apt-get install -f %s' % app_name],
        shell=True
    )
    result = proc.communicate()
    print(result)


def apt_remove(app_name):
    result = _pop(['apt-get remove -f %s' % app_name])
