"use strict";

const body_el = document.body;
const all_notion_el = document.getElementById('c-search-menu-breadcrum-all-notions');
const breadcrum_el = document.getElementById('c-search-menu-breadcrum');
const breadcrum_cat_el = document.getElementById('c-search-menu-breadcrum-cat');
const breadcrum_sub_cat_el = document.getElementById('c-search-menu-breadcrum-sub-cat');
const breadcrum_sub_sub_cat_el = document.getElementById('c-search-menu-breadcrum-sub-sub-cat');

const COMPONENT_APPS_CONTAINER = getClassEl('c-app__container');
const COMPONENT_MODAL_CONTAINER = getClassEl('c-modal__container');

function url_with_queries(url, params) {
    let query = Object.keys(params)
        .map(k => encodeURIComponent(k) + '=' + encodeURIComponent(params[k]))
        .join('&');
    return url + '?' + query;
}

function go_category(
    session,
    page_index,
    cat_name,
    sub_cat_name,
    sub_sub_cat_name
) {
    show_hide(all_notion_el, true);
    show_hide(breadcrum_el, true);
    show_hide(breadcrum_cat_el, false);
    show_hide(breadcrum_sub_cat_el, false);
    show_hide(breadcrum_sub_sub_cat_el, false);
    breadcrum_cat_el.innerText = cat_name;
    show_hide(breadcrum_cat_el, true, 'has-submenu');
    let params = {
        'session': session,
        'page_index': page_index
    };
    if (cat_name) {
        params['cat_name'] = cat_name;
        show_hide(all_notion_el, false);
        show_hide(breadcrum_cat_el, true);
    }
    if (sub_cat_name) {
        show_hide(breadcrum_cat_el, false, 'has-submenu');
        params['sub_cat_name'] = sub_cat_name;
    }
    breadcrum_sub_cat_el.innerText = sub_cat_name;
    if (sub_cat_name)
        show_hide(breadcrum_sub_cat_el, true);
    if (sub_sub_cat_name) {
        show_hide(breadcrum_sub_cat_el, false, 'has-submenu');
        params['sub_sub_cat_name'] = sub_sub_cat_name;
    }
    breadcrum_sub_sub_cat_el.innerText = sub_sub_cat_name;
    if (sub_sub_cat_name)
        show_hide(breadcrum_sub_sub_cat_el, true);
    fetch(url_with_queries('apps', params)).then(response => {
        response.text().then(value => {
            COMPONENT_APPS_CONTAINER.innerHTML = value;
        });
    });
}

function getClassEl(class_name) {
    return document.getElementsByClassName(class_name)[0];
}

function show_hide(component, value_exist, class_name=null) {
    if (!class_name)
        class_name = 'hidden';
    if (value_exist) {
        component.classList.remove(class_name);
        return;
    }
    component.classList.add(class_name);
}

function show_details(session, id) {
    let params = {
        'session': session
    };
    fetch(url_with_queries('app/' + id, params)).then(response => {
        response.text().then(value => {
            COMPONENT_MODAL_CONTAINER.innerHTML = value;
            c_modal__open();
        });
    });
}

function launch_app(id) {
    fetch('launch/' + id).then(response => {
        response.text().then(value => {
            console.log(value);
        });
    });
}
