"use strict";

const c_modal_mask_el = getClassEl('c-modal__mask');

const C_MODAL_MASK_APPLY_CLASS = 'c-modal__mask--apply';
const C_MODAL_HIDDEN_CLASS = 'c-modal__container--hidden';

function c_modal__open() {
    show_hide(c_modal_mask_el, false, C_MODAL_MASK_APPLY_CLASS);
    show_hide(COMPONENT_MODAL_CONTAINER, true, C_MODAL_HIDDEN_CLASS);
}

function c_modal__close() {
    show_hide(c_modal_mask_el, true, C_MODAL_MASK_APPLY_CLASS);
    show_hide(COMPONENT_MODAL_CONTAINER, false, C_MODAL_HIDDEN_CLASS);
}

document.onkeydown = function (e) {
    if (e.key == 'Escape')
        c_modal__close();
};
