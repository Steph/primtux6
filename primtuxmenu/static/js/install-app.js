const COMPONENT_SHELL_CODE = document.getElementById('shell-install-apps');

const ws_install = new WebSocket("ws://127.0.0.1:5000/ws/install");
const ws_uninstall = new WebSocket("ws://127.0.0.1:5000/ws/uninstall");

function install_app(id) {
    c_modal__close();
    ws_install.send(id);
    COMPONENT_SHELL_CODE.innerText = '';
}

ws_install.onmessage = function(event) {
    COMPONENT_SHELL_CODE.innerText += event.data.toString() + '\n';
};

ws_install.onerror = function(event) {
    console.log(event);
};

ws_install.onclose = function (event) {
    console.log(event);
};




function uninstall_app(id) {
    c_modal__close();
    ws_uninstall.send(id);
    COMPONENT_SHELL_CODE.innerText = '';
}

ws_uninstall.onmessage = function(event) {
    COMPONENT_SHELL_CODE.innerText += event.data.toString() + '\n';
};

ws_uninstall.onerror = function(event) {
    console.log(event);
};

ws_uninstall.onclose = function (event) {
    console.log(event);
};
