"use strict";

const search_menu = document.getElementById('c-search-menu');

const C_SEARCHMENU_OPEN_CLASS = 'c-search-menu__menu-container--open';

function open_search() {
    show_hide(
        search_menu,
        search_menu.classList.contains(C_SEARCHMENU_OPEN_CLASS),
        C_SEARCHMENU_OPEN_CLASS
    );
}

function close_search() {
    show_hide(search_menu, true, C_SEARCHMENU_OPEN_CLASS);
}

function c_menu_go_category(
    session,
    page_index,
    cat_name,
    sub_cat_name,
    sub_sub_cat_name
) {
    go_category(
        session,
        page_index,
        cat_name,
        sub_cat_name,
        sub_sub_cat_name
    );
    close_search();
}
